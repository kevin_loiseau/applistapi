# Symfony5 Docker Development Stack

### Prerequisites
* [Docker](https://www.docker.com/)

### Container
 - [nginx](https://pkgs.alpinelinux.org/packages?name=nginx&branch=v3.10) 1.18.+
 - [php-fpm](https://pkgs.alpinelinux.org/packages?name=php7&branch=v3.10) 7.4.+
    - [composer](https://getcomposer.org/) 
- [mysql](https://hub.docker.com/_/mysql/) 5.7.+

### Installing

run docker and connect to container:
```
 docker-compose up --build
```
modify your DATABASE_URL config in .env 
```
DATABASE_URL=mysql://root:root@mysql:3306/symfony?serverVersion=5.7
```
Enter in php volume

```
 docker-compose exec php sh
```

Get depandencies :
```
 composer install
```

Persist migrations:
```
 php bin/console doctrine:migrations:migrate
```

Generate Fake Datas with Fixtures :
```
 php bin/console doctrine:fixtures:load
```


Call [localhost](http://localhost/) to see all routes with swagger

[Gauthier Torreilles](https://www.linkedin.com/in/gauthier-torreilles/)

[Kévin Loiseau](https://www.linkedin.com/in/kevinloiseaudevphpsymfony/)
 
