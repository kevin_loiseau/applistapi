<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200924193255 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE friend_ship (id INT AUTO_INCREMENT NOT NULL, asker_id INT NOT NULL, receiver_id INT NOT NULL, receiver_aprouval TINYINT(1) NOT NULL, INDEX IDX_604A37C4CF34C66B (asker_id), INDEX IDX_604A37C4CD53EDB6 (receiver_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE item (id INT AUTO_INCREMENT NOT NULL, item_type_id INT DEFAULT NULL, libelle VARCHAR(255) NOT NULL, INDEX IDX_1F1B251ECE11AAC7 (item_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE item_type (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE list_shared_with (id INT AUTO_INCREMENT NOT NULL, shared_from_id INT NOT NULL, shared_to_id INT NOT NULL, list_shared_via_id INT NOT NULL, list_shared_id INT NOT NULL, INDEX IDX_ADD93A1D5919D5BC (shared_from_id), INDEX IDX_ADD93A1DCE5BA778 (shared_to_id), INDEX IDX_ADD93A1DBE6F83A (list_shared_via_id), INDEX IDX_ADD93A1D7658330B (list_shared_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE list_type (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE liste (id INT AUTO_INCREMENT NOT NULL, created_by_id INT NOT NULL, list_type_id INT NOT NULL, created_at DATETIME DEFAULT NULL, content JSON NOT NULL, INDEX IDX_FCF22AF4B03A8386 (created_by_id), INDEX IDX_FCF22AF41903519E (list_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE share_way (id INT AUTO_INCREMENT NOT NULL, way VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, email LONGTEXT NOT NULL, password LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE friend_ship ADD CONSTRAINT FK_604A37C4CF34C66B FOREIGN KEY (asker_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE friend_ship ADD CONSTRAINT FK_604A37C4CD53EDB6 FOREIGN KEY (receiver_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE item ADD CONSTRAINT FK_1F1B251ECE11AAC7 FOREIGN KEY (item_type_id) REFERENCES item_type (id)');
        $this->addSql('ALTER TABLE list_shared_with ADD CONSTRAINT FK_ADD93A1D5919D5BC FOREIGN KEY (shared_from_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE list_shared_with ADD CONSTRAINT FK_ADD93A1DCE5BA778 FOREIGN KEY (shared_to_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE list_shared_with ADD CONSTRAINT FK_ADD93A1DBE6F83A FOREIGN KEY (list_shared_via_id) REFERENCES share_way (id)');
        $this->addSql('ALTER TABLE list_shared_with ADD CONSTRAINT FK_ADD93A1D7658330B FOREIGN KEY (list_shared_id) REFERENCES liste (id)');
        $this->addSql('ALTER TABLE liste ADD CONSTRAINT FK_FCF22AF4B03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE liste ADD CONSTRAINT FK_FCF22AF41903519E FOREIGN KEY (list_type_id) REFERENCES list_type (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE item DROP FOREIGN KEY FK_1F1B251ECE11AAC7');
        $this->addSql('ALTER TABLE liste DROP FOREIGN KEY FK_FCF22AF41903519E');
        $this->addSql('ALTER TABLE list_shared_with DROP FOREIGN KEY FK_ADD93A1D7658330B');
        $this->addSql('ALTER TABLE list_shared_with DROP FOREIGN KEY FK_ADD93A1DBE6F83A');
        $this->addSql('ALTER TABLE friend_ship DROP FOREIGN KEY FK_604A37C4CF34C66B');
        $this->addSql('ALTER TABLE friend_ship DROP FOREIGN KEY FK_604A37C4CD53EDB6');
        $this->addSql('ALTER TABLE list_shared_with DROP FOREIGN KEY FK_ADD93A1D5919D5BC');
        $this->addSql('ALTER TABLE list_shared_with DROP FOREIGN KEY FK_ADD93A1DCE5BA778');
        $this->addSql('ALTER TABLE liste DROP FOREIGN KEY FK_FCF22AF4B03A8386');
        $this->addSql('DROP TABLE friend_ship');
        $this->addSql('DROP TABLE item');
        $this->addSql('DROP TABLE item_type');
        $this->addSql('DROP TABLE list_shared_with');
        $this->addSql('DROP TABLE list_type');
        $this->addSql('DROP TABLE liste');
        $this->addSql('DROP TABLE share_way');
        $this->addSql('DROP TABLE user');
    }
}
