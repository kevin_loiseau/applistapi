<?php


namespace App\Controller;
use App\Repository\FriendShipRepository;
use App\Repository\UserRepository;
use App\Service\FriendShipService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use \Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class FriendShipController
 *
 * @package                   App\Controller
 * @Route("/api/friend_ship")
 */
class FriendShipController extends AbstractController
{

    /**
     * @var FriendShipService
     */
    private FriendShipService $friendShipService;

    public function __construct(FriendShipService $friendShipService)
    {
        $this->friendShipService = $friendShipService;
    }

    /**
     * @Route("/user_firend_ship", name="friend_ship_user", methods={"GET"})
     * @param NormalizerInterface $normalizer
     * @param FriendShipRepository $friendShipRepository
     * @param UserRepository $userRepository
     * @return Response
     * @throws ExceptionInterface
     */
    public function getFriendShipsByUser(
                                         NormalizerInterface $normalizer,
                                         FriendShipRepository $friendShipRepository,
                                         UserRepository $userRepository){

        return $this->friendShipService->getFriendShipByUser($normalizer, $friendShipRepository, $userRepository,$this->getUser());
    }

    /**
     * @Route("", name="friend_ship_new", methods={"POST"})
     * @param     Request              $request
     * @param     NormalizerInterface  $normalizer
     * @param     UserRepository       $userRepository
     * @param     FriendShipRepository $friendShipRepository
     * @return    Response
     * @throws    ExceptionInterface
     */
    public function new(Request $request,
        NormalizerInterface $normalizer,
        UserRepository $userRepository,
        FriendShipRepository $friendShipRepository
    ):Response {
        return $this->friendShipService->newFriendShip(
            $request,
            $normalizer,
            $userRepository,
            $this->getDoctrine()->getManager(),
            $this->getUser(), $friendShipRepository
        );
    }

    /**
     * @Route("/{id}", name="friend_ship_eddit", methods={"PUT"})
     * @param          Request              $request
     * @param          NormalizerInterface  $normalizer
     * @param          FriendShipRepository $friendShipRepository
     * @param          int                  $id
     * @return         Response
     * @throws         ExceptionInterface
     */
    public function edit(Request $request,
        NormalizerInterface $normalizer,
        FriendShipRepository $friendShipRepository,
        int $id
    ):Response {
        return $this->friendShipService->editFriendShip($request, $friendShipRepository, $normalizer, $this->getDoctrine()->getManager(), $id);
    }

    /**
     * @Route("/{id}", name="friend_ship_delete", methods={"DELETE"})
     * @param          FriendShipRepository $friendShipRepository
     * @param          int                  $id
     * @return         Response
     */
    public function delete(FriendShipRepository $friendShipRepository,  int $id):Response
    {
        return $this->friendShipService->deleteList($friendShipRepository, $this->getDoctrine()->getManager(), $id);
    }
}