<?php

namespace App\Controller;

use App\Repository\ListeRepository;
use App\Repository\ListTypeRepository;
use App\Repository\UserRepository;
use Doctrine\DBAL\Driver\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Service\ListService;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;


/**
 * @Route("/api/list")
 */
class ListController extends AbstractController
{
    /**
     * @var ListService
     */
    private ListService $listService;

    public function __construct(ListService $listService)
    {

        $this->listService = $listService;
    }

    /**
     * @Route("", name="lists_show", methods={"GET"})
     * @param     NormalizerInterface $normalizer
     * @param     ListeRepository     $listeRepository
     * @return    Response
     * @throws    ExceptionInterface
     */
    public function index(NormalizerInterface $normalizer,
        ListeRepository $listeRepository
    ):Response {
        return $this->listService->getAllLists($listeRepository, $normalizer);
    }

    /**
     * @Route("/user_lists", name="user_lists", methods={"GET"})
     * @param                ListeRepository     $listeRepository
     * @param                NormalizerInterface $normalizer
     * @param                UserRepository      $userRepository
     * @param                ListTypeRepository  $typeRepository
     * @return               Response
     * @throws               Exception
     * @throws               ExceptionInterface
     */
    public function getListsByUser(ListeRepository $listeRepository,
        NormalizerInterface $normalizer,
        UserRepository $userRepository,
        ListTypeRepository $typeRepository
    ):Response {
        return $this->listService->getListsByUser($listeRepository, $this->getUser(), $normalizer);
    }

    /**
     * @Route("/{id}", name="list_show", methods={"GET"})
     * @param          NormalizerInterface $normalizer
     * @param          ListeRepository     $listeRepository
     * @param          int                 $id
     * @return         Response
     * @throws         ExceptionInterface|Exception
     */
    public function show(NormalizerInterface $normalizer,
        ListeRepository $listeRepository,
        int $id
    ) :Response {
        return $this->listService->getList($listeRepository, $normalizer, $id, $this->getUser());
    }

    /**
     * @Route("", name="list_new", methods={"POST"})
     * @param     Request             $request
     * @param     NormalizerInterface $normalizer
     * @param     ListTypeRepository  $listTypeRepository
     * @return    Response
     * @throws    ExceptionInterface
     */
    public function new(Request $request,
        NormalizerInterface $normalizer,
        ListTypeRepository $listTypeRepository
    ): Response {
        return $this->listService->newList($request, $normalizer, $this->getDoctrine()->getManager(), $this->getUser(), $listTypeRepository);
    }

    /**
     * @Route("/{id}", name="list_eddit", methods={"PUT"})
     * @param          Request             $request
     * @param          NormalizerInterface $normalizer
     * @param          ListeRepository     $listeRepository
     * @param          ListTypeRepository  $listTypeRepository
     * @param          int                 $id
     * @return         Response
     * @throws         ExceptionInterface
     */
    public function edit(Request $request,
        NormalizerInterface $normalizer,
        ListeRepository $listeRepository,
        ListTypeRepository $listTypeRepository,
        int $id
    ):Response {
        return $this->listService->editList($request, $listeRepository, $normalizer, $this->getDoctrine()->getManager(), $listTypeRepository, $id, $this->getUser());
    }

    /**
     * @Route("/{id}", name="list_delete", methods={"DELETE"})
     * @param          ListeRepository $listeRepository
     * @param          int             $id
     * @return         Response
     */
    public function delete(ListeRepository $listeRepository, int $id):Response
    {
        return $this->listService->deleteList($listeRepository, $this->getDoctrine()->getManager(), $id, $this->getUser());
    }

}
