<?php


namespace App\Controller;
use App\Mailing\Mailer;
use App\Repository\FriendShipRepository;
use App\Repository\ListeRepository;
use App\Repository\ListSharedWithRepository;
use App\Repository\ShareWayRepository;
use App\Repository\UserRepository;
use App\Service\ListSharingService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;


/**
 * @Route("/api/list_share")
 */
class ListShareController extends AbstractController
{
    /**
     * @Route("", name="list_share_new", methods={"POST"})
     * @param     ListSharingService       $listSharingService
     * @param     Request                  $request
     * @param     NormalizerInterface      $normalizer
     * @param     UserRepository           $userRepository
     * @param     ListSharedWithRepository $listSharedWithRepository
     * @param     ShareWayRepository       $shareWayRepository
     * @param     ListeRepository          $listeRepository
     * @param     FriendShipRepository     $friendShipRepository
     * @param     Mailer                   $mailer
     * @return    Response
     */
    public function new(ListSharingService $listSharingService, Request $request,
        NormalizerInterface $normalizer,
        UserRepository $userRepository,
        ListSharedWithRepository $listSharedWithRepository,
        ShareWayRepository $shareWayRepository,
        ListeRepository $listeRepository,
        FriendShipRepository $friendShipRepository,
        Mailer $mailer
    ):Response {
        return $listSharingService->newListSharing(
            $request,
            $normalizer,
            $userRepository,
            $listSharedWithRepository,
            $shareWayRepository,
            $listeRepository,
            $friendShipRepository,
            $this->getDoctrine()->getManager(),
            $this->getUser(),
            $mailer
        );
    }
}