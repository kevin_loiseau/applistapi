<?php

namespace App\Controller;

use App\Mailing\Mailer;
use App\Repository\UserRepository;
use App\Service\RegistrationService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class RegistrationController extends AbstractFOSRestController
{
    /**
     * @Route("/register", name="register", methods={"POST"})
     * @param              Request                      $request
     * @param              UserRepository               $userRepository
     * @param              UserPasswordEncoderInterface $passwordEncoder
     * @param              NormalizerInterface          $normalizer
     * @param              Mailer                       $mailer
     * @param              RegistrationService          $service
     * @return             Response
     * @throws             ExceptionInterface
     */
    public function register(Request $request,
        UserRepository $userRepository,
        UserPasswordEncoderInterface $passwordEncoder,
        NormalizerInterface $normalizer, Mailer $mailer, RegistrationService $service
    ): Response {

        return $service->newRegistration($request, $userRepository, $this->getDoctrine()->getManager(), $passwordEncoder, $normalizer, $mailer);
    }
}
