<?php

namespace App\DataFixtures;

use App\Entity\ListType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use Faker\Factory;
use App\Entity\ShareWay;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $generator = Factory::create('fr_FR');
        $types = ["Liste de course", "Liste d'invités", "Liste de Tâches"];
        for ($j=0; $j<sizeof($types); ++$j){
            $listType = new ListType($types[$j]);
            $manager->persist($listType);
        }

        $shareWays = ["mail", "profile utilisateur"];
        for ($k=0;$k<sizeof($shareWays); ++$k){
            $shareWay = new ShareWay($shareWays[$k]);
            $manager->persist($shareWay);
        }
        $gauthier = new User();
        $gauthier->setFirstName("Gauthier");
        $gauthier->setLastName("Torreilles");
        $gauthier->setEmail("gauthiertorreilles@gmail.com");
        $gauthier->setPassword("pass");
        $manager->persist($gauthier);

        $kevin = new User();
        $kevin->setFirstName("Kévin");
        $kevin->setLastName("Loiseau");
        $kevin->setEmail("kevinpellin521@gmail.com");
        $kevin->setPassword("pass");
        $manager->persist($kevin);

        for ($i=0; $i<50; ++$i){
            $user = new User();
            $user->setFirstName($generator->firstName)
                ->setLastName($generator->lastName)
                ->setEmail($generator->email)
                ->setPassword('pass');
            $manager->persist($user);
        }
        $manager->flush();
    }
}
