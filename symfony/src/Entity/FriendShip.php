<?php

namespace App\Entity;

use App\Repository\FriendShipRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FriendShipRepository::class)
 */
class FriendShip
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private User $asker;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private User $receiver;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $receiverAprouval;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return User|null
     */
    public function getAsker(): ?User
    {
        return $this->asker;
    }

    /**
     * @param  User $asker
     * @return $this
     */
    public function setAsker(User $asker): self
    {
        $this->asker = $asker;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getReceiver(): ?User
    {
        return $this->receiver;
    }

    /**
     * @param  User $receiver
     * @return $this
     */
    public function setReceiver(User $receiver): self
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getReceiverAprouval(): ?bool
    {
        return $this->receiverAprouval;
    }

    /**
     * @param  bool $receiverAprouval
     * @return $this
     */
    public function setReceiverAprouval(bool $receiverAprouval): self
    {
        $this->receiverAprouval = $receiverAprouval;

        return $this;
    }
}
