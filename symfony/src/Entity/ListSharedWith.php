<?php

namespace App\Entity;

use App\Repository\ListSharedWithRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ListSharedWithRepository::class)
 */
class ListSharedWith
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private User $sharedFrom;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private User $sharedTo;

    /**
     * @ORM\ManyToOne(targetEntity=ShareWay::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private ShareWay $listSharedVia;

    /**
     * @ORM\ManyToOne(targetEntity=Liste::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private Liste $listShared;

    /**
     * ListSharedWith constructor.
     *
     * @param User     $sharedFrom
     * @param User     $sharedTo
     * @param ShareWay $shareWay
     * @param Liste    $liste
     */
    public function __construct(User $sharedFrom, User $sharedTo, ShareWay $shareWay, Liste $liste)
    {
        $this->sharedFrom = $sharedFrom;
        $this->sharedTo = $sharedTo;
        $this->listSharedVia = $shareWay;
        $this->listShared = $liste;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return User|null
     */
    public function getSharedFrom(): ?User
    {
        return $this->sharedFrom;
    }

    /**
     * @param  User $sharedFrom
     * @return $this
     */
    public function setSharedFrom(User $sharedFrom): self
    {
        $this->sharedFrom = $sharedFrom;

        return $this;
    }

    /**
     * @return User|null
     */
    public function getSharedTo(): ?User
    {
        return $this->sharedTo;
    }

    /**
     * @param  User $sharedTo
     * @return $this
     */
    public function setSharedTo(User $sharedTo): self
    {
        $this->sharedTo = $sharedTo;

        return $this;
    }

    /**
     * @return ShareWay|null
     */
    public function getListSharedVia(): ?ShareWay
    {
        return $this->listSharedVia;
    }

    /**
     * @param  ShareWay $listSharedVia
     * @return $this
     */
    public function setListSharedVia(ShareWay $listSharedVia): self
    {
        $this->listSharedVia = $listSharedVia;

        return $this;
    }

    /**
     * @return Liste|null
     */
    public function getListShared(): ?Liste
    {
        return $this->listShared;
    }

    /**
     * @param  Liste $listShared
     * @return $this
     */
    public function setListShared(Liste $listShared): self
    {
        $this->listShared = $listShared;

        return $this;
    }
}
