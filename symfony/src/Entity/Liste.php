<?php

namespace App\Entity;

use App\Repository\ListeRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ListeRepository::class)
 */
class Liste
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="lists")
     * @ORM\JoinColumn(nullable=false)
     */
    private User $createdBy;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private DateTime $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=ListType::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private ListType $listType;

    /**
     * @ORM\Column(type="json")
     */
    private array $content = [];


    public function __construct(User $createdBy, DateTime $createdAt, ListType $listType, array $content)
    {
        $this->createdBy = $createdBy;
        $this->createdAt = $createdAt;
        $this->listType = $listType;
        $this->content = $content;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param  int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    /**
     * @param  User $createdBy
     * @return Liste|null ?$this
     */
    public function setCreatedBy(User $createdBy): ?self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param  DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt(DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return ListType|null
     */
    public function getListType(): ?ListType
    {
        return $this->listType;
    }

    /**
     * @param  ListType $listType
     * @return $this
     */
    public function setListType(ListType $listType): self
    {
        $this->listType = $listType;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getContent(): ?array
    {
        return $this->content;
    }

    /**
     * @param  array $content
     * @return $this
     */
    public function setContent(array $content): self
    {
        $this->content = $content;

        return $this;
    }
}
