<?php

namespace App\Entity;

use App\Repository\ShareWayRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ShareWayRepository::class)
 */
class ShareWay
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $way;

    public function __construct(string $way)
    {
        return $this->way = $way;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getWay(): ?string
    {
        return $this->way;
    }

    /**
     * @param  string $way
     * @return $this
     */
    public function setWay(string $way): self
    {
        $this->way = $way;

        return $this;
    }
}
