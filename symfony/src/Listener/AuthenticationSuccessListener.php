<?php

namespace App\Listener;

use Exception;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

/**
 * Stockage du token dans un cookie au moment de l'authentification d'un utilisateur
 * Class AuthenticationSuccessListener
 *
 * @package App\Listener
 */
class AuthenticationSuccessListener
{

    private bool $secure = false;
    private string $jwtTokenTtl;

    public function __construct($ttl)
    {
        $this->jwtTokenTtl = $ttl;
    }

    /**
     * @param  AuthenticationSuccessEvent $event
     * @return Response
     * @throws Exception
     */
    public function onAuthenticationSuccess(AuthenticationSuccessEvent $event)
    {

        $response = $event->getResponse();
        $data = $event->getData();
        $tokenJWT = $data["token"];
        unset($data['token']);
        $event->setData($data);

        $response->headers->setCookie(
            new Cookie(
                'BEARER', $tokenJWT, (
                new \DateTime())
                    ->add(new \DateInterval('PT' . $this->jwtTokenTtl . 'S')), '/', null, $this->secure
            )
        );

        return $response;
    }
}