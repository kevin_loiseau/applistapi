<?php

namespace App\Mailing;

use App\Entity\ListSharedWith;
use App\Entity\User;
use App\Repository\UserRepository;
use Swift_Mailer;
use Twig\Environment;

class Mailer
{

    private Swift_Mailer $mailer;
    /**
     * @var Environment
     */
    private Environment $renderer;

    public function __construct(Swift_Mailer $mailer, Environment $renderer)
    {

        $this->mailer = $mailer;
        $this->renderer = $renderer;
    }

    public function notify(ListSharedWith $sharedWith)
    {
        $domPdf = new PdfService();
        $pdfFilePath = $domPdf->pdfBuilder($sharedWith, $this->renderer);
        $message = (new \Swift_Message($sharedWith->getSharedFrom()->getFirstName() . $sharedWith->getSharedFrom()->getLastName() . ' vous a envoyé une liste'))
            ->setFrom($sharedWith->getSharedFrom()->getEmail())
            ->setTo($sharedWith->getSharedTo()->getEmail())
            ->setCharset('utf-8')
            ->setBody(
                $this->renderer->render(
                    'emails/list.html.twig', [
                        'sharedWith' => $sharedWith
                    ]
                ), 'text/html'
            )
            ->attach(\Swift_Attachment::fromPath($pdfFilePath));

        $this->mailer->send($message);
    }

    public function registrationConfirmation(User $user)
    {
        $message = (new \Swift_Message('La création de votre compte AppList'))
            ->setFrom('contact.applist@gmail.com')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderer->render(
                    'emails/register.html.twig', [
                    'user' => $user
                    ]
                ), 'text/html'
            );
        $this->mailer->send($message);
    }
}