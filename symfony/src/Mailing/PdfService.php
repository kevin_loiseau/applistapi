<?php
namespace App\Mailing;

use App\Entity\ListSharedWith;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Component\Filesystem\Filesystem;
use Twig\Environment;

class PdfService
{
    public function pdfBuilder(ListSharedWith $sharedWith, Environment $renderer)
    {
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        $dompdf = new Dompdf($pdfOptions);
        $html = $renderer->render(
            'emails/list.html.twig', [
                'sharedWith' => $sharedWith
            ]
        );
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();
        $output = $dompdf->output();
        $pdfFilepath =  '../public/pdf/liste.pdf';
        $filesystem = new Filesystem();
        if ($filesystem->exists($pdfFilepath)) {
            $filesystem->remove([$pdfFilepath]);
        }
        file_put_contents($pdfFilepath, $output);
        return $pdfFilepath;
    }
}