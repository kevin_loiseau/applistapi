<?php

namespace App\Repository;

use App\Entity\FriendShip;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FriendShip|null find($id, $lockMode = null, $lockVersion = null)
 * @method FriendShip|null findOneBy(array $criteria, array $orderBy = null)
 * @method FriendShip[]    findAll()
 * @method FriendShip[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FriendShipRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FriendShip::class);
    }

    // /**
    //  * @return FriendShip[] Returns an array of FriendShip objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FriendShip
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     *
     * @param  User $asker
     * @param  User $receiver
     * @return FriendShip|null
     * @throws NonUniqueResultException
     */
    public function findOneActiveFriendShipByUsers(User $asker, User $receiver):?FriendShip
    {
        return $this->createQueryBuilder('f')
            ->setParameter('receiver', $receiver)
            ->setParameter('asker', $asker)
            ->where('f.receiverAprouval = 1')
            ->andWhere('f.asker = :asker AND f.receiver = :receiver OR f.asker = :receiver AND f.receiver = :asker')
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param User $user
     * @return array
     * @throws Exception|\Doctrine\DBAL\Driver\Exception
     */
    public function findFriendShipByUser(User $user):array {
        $id = $user->getId();
        $connexion = $this->getEntityManager()->getConnection();
        $statement = $connexion->prepare("select (CASE 
                            when asker_id = :userID then receiver_id
                            when receiver_id = :userID then asker_id 
                        END
                    )
                    from friend_ship fs 
                    where receiver_aprouval = 1
                    and (asker_id = :userID or receiver_id = :userID )");
        $statement->bindParam(":userID", $id);
        $statement->execute();
        return $statement->fetchAllAssociative();



    }
}
