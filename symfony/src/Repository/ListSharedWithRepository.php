<?php

namespace App\Repository;

use App\Entity\ListSharedWith;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ListSharedWith|null find($id, $lockMode = null, $lockVersion = null)
 * @method ListSharedWith|null findOneBy(array $criteria, array $orderBy = null)
 * @method ListSharedWith[]    findAll()
 * @method ListSharedWith[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ListSharedWithRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ListSharedWith::class);
    }

    // /**
    //  * @return ListSharedWith[] Returns an array of ListSharedWith objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ListSharedWith
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
