<?php

namespace App\Repository;

use App\Entity\Liste;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Statement;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\ListType;
use App\Entity\ListSharedWith;

/**
 * @method Liste|null find($id, $lockMode = null, $lockVersion = null)
 * @method Liste|null findOneBy(array $criteria, array $orderBy = null)
 * @method Liste[]    findAll()
 * @method Liste[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ListeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Liste::class);
    }


    /**
     * @param  User $user
     * @return array
     * @throws Exception|\Doctrine\DBAL\Driver\Exception
     */
    public function findListsByUser(User $user)
    {
        $id= $user->getId();
        $connexion = $this->getEntityManager()->getConnection();
        $statement = $connexion->prepare("SELECT DISTINCT l.id, l.content, l.list_type_id, l.created_at, l.created_by_id 
FROM liste l 
left join list_shared_with lsw on l.id = lsw.list_shared_id
left join share_way sw on lsw.list_shared_via_id = sw.id
where l.created_by_id = :userID OR lsw.shared_to_id = :userID and sw.way = \"profil utilisateur\"");
        $statement->bindParam(":userID", $id);
        $statement->execute();
        return $statement->fetchAllAssociative();

    }

    // /**

    //  * @return Liste[] Returns an array of Liste objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Liste
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
