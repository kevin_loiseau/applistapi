<?php

namespace App\Repository;

use App\Entity\ShareWay;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ShareWay|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShareWay|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShareWay[]    findAll()
 * @method ShareWay[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShareWayRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShareWay::class);
    }

    // /**
    //  * @return ShareWay[] Returns an array of ShareWay objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ShareWay
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
