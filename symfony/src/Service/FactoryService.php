<?php


namespace App\Service;


use App\Entity\Liste;
use App\Entity\ListType;
use App\Entity\User;
use App\Entity\FriendShip;
use App\Entity\ListSharedWith;
use App\Entity\ShareWay;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use function Symfony\Component\DependencyInjection\Loader\Configurator\service_locator;

class FactoryService
{
    /**
     * @param  $toSerialize
     * @param  int $statusCode
     * @return Response
     */
    public function responseFactory($toSerialize, int $statusCode):Response
    {
        $response = new Response();
        $jsonMessage = json_encode($toSerialize);
        $response->setContent($jsonMessage);
        $response->setStatusCode($statusCode);
        $response->setCharset('utf-8');
        $response->headers->set("Content-Type", "application/json");
        return $response;
    }

    /**
     * @param  Liste               $list
     * @param  NormalizerInterface $normalizer
     * @return array
     * @throws ExceptionInterface
     */
    public function listFactory(Liste $list, NormalizerInterface $normalizer):array
    {
        return $normalizer->normalize(
            [
            "id" => $list->getId(),
            "content" => $list->getContent(),
            "createdBy" => self::userFactory($list->getCreatedBy(), $normalizer),
            "createdAt" => $list->getCreatedAt(),
            "listType" => self::listTypeFactory($list->getListType(), $normalizer)
            ]
        );
    }

    /**
     * @param  array               $lists
     * @param  NormalizerInterface $normalizer
     * @return array
     * @throws ExceptionInterface
     */
    public function listsFactory(array $lists, NormalizerInterface $normalizer):array
    {
        $listsNormalized = [];
        foreach ($lists as $list){
            $listsNormalized[] = self::listFactory($list, $normalizer);
        }
        return $listsNormalized;
    }

    /**
     * @param  ListType            $listType
     * @param  NormalizerInterface $normalizer
     * @return array
     * @throws ExceptionInterface
     */
    public function listTypeFactory(ListType $listType, NormalizerInterface $normalizer):array
    {
        return $normalizer->normalize(
            [
            "id" => $listType->getId(),
            "libelle" => $listType->getLibelle(),
            "createdBy" => is_null($listType->getCreatedBy()) ? "" : self::userFactory($listType->getCreatedBy(), $normalizer)
            ]
        );
    }

    /**
     * @param  User                $user
     * @param  NormalizerInterface $normalizer
     * @return array
     * @throws ExceptionInterface
     */
    public function userFactory(User $user, NormalizerInterface $normalizer):array
    {
        return $normalizer->normalize(
            [
            "id" => $user->getId(),
            "firstName" => $user->getFirstName(),
            "lastName" => $user->getLastName(),
            "email" => $user->getEmail()
            ]
        );
    }

    /**
     * @param array $users
     * @param NormalizerInterface $normalizer
     * @return array
     * @throws ExceptionInterface
     */
    public function usersFactory(array $users, NormalizerInterface $normalizer):array {
        $usersNormalized = [];
        foreach ($users as $user){
            $usersNormalized[] = self::userFactory($user, $normalizer);
        }
        return $usersNormalized;
    }

    /**
     * @param array $friendShips
     * @param NormalizerInterface $normalizer
     * @param string $message
     * @return array
     * @throws ExceptionInterface
     */
    public function friendShipsfactory( array $friendShips,
                                      NormalizerInterface $normalizer, string $message):array {
        $friendShipsNormalized = [];
        foreach ($friendShips as $friendShip){
            $friendShipsNormalized[] = self::friendShipfactory($friendShip, $normalizer, $message);
        }
        return $friendShipsNormalized;
    }

    /**
     * @param  FriendShip          $friendShip
     * @param  NormalizerInterface $normalizer
     * @param  string              $message
     * @return array
     * @throws ExceptionInterface
     */
    public function friendShipfactory(FriendShip $friendShip,
        NormalizerInterface $normalizer,
        string $message
    ):array {
        return $normalizer->normalize(
            [
            "id" => $friendShip->getId(),
            "asker" => self::userFactory($friendShip->getAsker(), $normalizer),
            "receiver" => self::userFactory($friendShip->getReceiver(), $normalizer),
            "receiverApprouval" => $friendShip->getReceiverAprouval(),
            "message" => $message
            ]
        );
    }

    /**
     * @param  ListSharedWith      $listSharedWith
     * @param  NormalizerInterface $normalizer
     * @return array
     * @throws ExceptionInterface
     */
    public function sharingList(ListSharedWith $listSharedWith,
        NormalizerInterface $normalizer
    ):array {
        return $normalizer->normalize(
            [
            "id"=>$listSharedWith->getId(),
            "sharedFrom"=>self::userFactory($listSharedWith->getSharedFrom(), $normalizer),
            "sharedTo"=>self::userFactory($listSharedWith->getSharedTo(), $normalizer),
            "listShared" => self::listFactory($listSharedWith->getListShared(), $normalizer),
            "sharedVia"=>self::shareWayFactory($listSharedWith->getListSharedVia(), $normalizer)
            ]
        );
    }

    /**
     * @param  ShareWay            $shareWay
     * @param  NormalizerInterface $normalizer
     * @return array
     * @throws ExceptionInterface
     */
    public function shareWayFactory(ShareWay $shareWay, NormalizerInterface $normalizer):array
    {
        return $normalizer->normalize(
            [
            "id"=>$shareWay->getId(),
            "way"=>$shareWay->getWay()
            ]
        );
    }
}