<?php


namespace App\Service;
use App\Entity\FriendShip;
use App\Entity\User;
use App\Repository\FriendShipRepository;
use App\Repository\UserRepository;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class FriendShipService
{
    /**
     * @var FactoryService
     */
    private FactoryService $factoryService;

    public function __construct(FactoryService $factoryService)
    {

        $this->factoryService = $factoryService;
    }

    /**
     * @param NormalizerInterface $normalizer
     * @param FriendShipRepository $friendShipRepository
     * @param UserRepository $userRepository
     * @param User $user
     * @return Response
     * @throws ExceptionInterface
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public function getFriendShipByUser(NormalizerInterface $normalizer,
                                        FriendShipRepository $friendShipRepository,
                                        UserRepository $userRepository,
                                        $user){
        try {
            $friends = UserService::arrayIdToArrayUsers($friendShipRepository->findFriendShipByUser($user), $userRepository);
            if (!empty($friends)){
                $response = $this->factoryService->responseFactory($this->factoryService->usersFactory($friends, $normalizer), 200);
            }else{
                $message = "Vous n'avez pas encore d'amis.";
                $response =  $response = $this->factoryService->responseFactory($message, 200);
            }
        }catch (\Exception $exception){
            $message = "Il y a eu un problème.";
            $response = $this->factoryService->responseFactory($message, 400);
        } finally {
            return $response;
        }

    }


    /**
     * @param  Request              $request
     * @param  NormalizerInterface  $normalizer
     * @param  UserRepository       $userRepository
     * @param  $entityManager
     * @param  UserInterface        $asker
     * @param  FriendShipRepository $friendShipRepository
     * @return Response
     * @throws ExceptionInterface
     */
    public function newFriendShip(Request $request,
        NormalizerInterface $normalizer,
        UserRepository $userRepository,
        $entityManager, UserInterface $asker, FriendShipRepository $friendShipRepository
    ):Response {
        try {
            $data = json_decode($request->getContent(), true);
            $receiver = $userRepository->findOneBy(['email'=>$data['emailReceiver']]);
            if (!empty($receiver) && !is_null($receiver)) {
                if (is_null($friendShipRepository->findOneBy(["asker"=>$asker, "receiver"=>$receiver])) 
                    && is_null($friendShipRepository->findOneBy(["asker" =>$receiver, "receiver"=>$asker])) 
                ) {
                    $friendShip = new FriendShip();
                    $friendShip->setAsker($asker);
                    $friendShip->setReceiver($receiver);
                    $friendShip->setReceiverAprouval(false);
                    $entityManager->persist($friendShip);
                    $entityManager->flush();
                    $message = $asker." a envoyé une demande d'ami à ".$receiver;
                    $response = $this->factoryService->responseFactory($this->factoryService->friendShipfactory($friendShip, $normalizer, $message), 200);
                }else{
                    $message = "Vous avez déjà demandé ". $receiver." en ami.";
                    $response = $this->factoryService->responseFactory($message, 403);
                }
            }else{
                $message = "Il n'existe pas d'utilisateur avec l'email ".$data['emailReceiver']. ".";
                $response = $this->factoryService->responseFactory($message, 404);
            }
        }catch (\Exception $exception){
            $message = "Il y a eu un problème lors de l'exécution de la requête, veuillez réessayer plus tard.";
            $response = $this->factoryService->responseFactory($message, 415);
        } finally {
            return $response;
        }
    }

    /**
     * @param  Request              $request
     * @param  FriendShipRepository $friendShipRepository
     * @param  NormalizerInterface  $normalizer
     * @param  $entityManager
     * @param  int                  $id
     * @return Response
     * @throws ExceptionInterface
     */
    public function editFriendShip(Request $request,
        FriendShipRepository $friendShipRepository,
        NormalizerInterface $normalizer,
        $entityManager, int $id
    ):Response {
        try {
            $data = json_decode($request->getContent(), true);
            $friendShip = $friendShipRepository->findOneBy(['id'=>$id]);
            if (!is_null($friendShip)) {
                if (!self::verifyFriendShip($friendShip->getAsker(), $friendShip->getReceiver(), $friendShipRepository)) {
                    $receiverApprouval = $data['receiverApprouval'];
                    $friendShip->setReceiverAprouval($receiverApprouval);
                    $message = self::friendShipRequestMessage($receiverApprouval, $friendShip);
                    $entityManager->flush();
                    $response = $this->factoryService->responseFactory($this->factoryService->friendShipfactory($friendShip, $normalizer, $message), 200);
                }else{
                    $message = "Attention, ".$friendShip->getAsker()." et vous êtes déjà amis";
                    $response = $this->factoryService->responseFactory($message, 403);
                }
            }else{
                $message = "Cette demande d'ami n'existe pas";
                $response = $this->factoryService->responseFactory($message, 404);
            }

        }catch (\Exception $exception){
            $message = "Il y a eu un problème";
            $response = $this->factoryService->responseFactory($message, 415);
        } finally {
            return $response;
        }
    }

    /**
     * @param  FriendShipRepository $friendShipRepository
     * @param  $entityManger
     * @param  int                  $id
     * @return Response
     */
    public function deleteList(FriendShipRepository $friendShipRepository, $entityManger, int $id):Response
    {
        try {
            $friendShip = $friendShipRepository->findOneBy(['id'=>$id]);
            if (!empty($friendShip)&&!is_null($friendShip)) {
                $asker = $friendShip->getAsker();
                $receiver = $friendShip->getReceiver();
                $entityManger->remove($friendShip);
                $entityManger->flush();
                $message = "La relation entre ".$asker." et ".$receiver." a été supprimée";
                $response = $this->factoryService->responseFactory($message, 200);
            }else{
                $message = "Il n'existe pas de relation sous cet identifiant";
                $response = $this->factoryService->responseFactory($message, 404);
            }
        }catch (\Exception $exception){
            $message = "Il y a eu un problème";
            $response = $this->factoryService->responseFactory($message, 415);
        } finally {
            return $response;
        }
    }

    /**
     * @param  bool       $approuval
     * @param  FriendShip $friendShip
     * @return string
     */
    private function friendShipRequestMessage(bool $approuval, FriendShip $friendShip):string
    {
        $answer = $approuval ? $answer = "a accepté" : $answer = "a refusé";
        $space = " ";
        $message = $friendShip->getReceiver();
        $message .= $space;
        $message .= $answer;
        $message .= $space;
        $message .= "la demande d'ami de";
        $message .= $space;
        $message .= $friendShip->getAsker();
        return $message;
    }

    /**
     * @param  User                 $user1
     * @param  User                 $user2
     * @param  FriendShipRepository $friendShipRepository
     * @return bool
     * @throws NonUniqueResultException
     */
    public function verifyFriendShip(User $user1,
        User $user2,
        FriendShipRepository $friendShipRepository
    ):bool {
         !is_null($friendShipRepository->findOneActiveFriendShipByUsers($user1, $user2)) ? $result = true : $result = false;
         return $result;
    }
}