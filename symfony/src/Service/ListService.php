<?php
namespace App\Service;
use App\Entity\Liste;
use App\Entity\User;
use App\Repository\ListeRepository;
use App\Repository\ListTypeRepository;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\DBAL\Driver\Exception;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ListService
{
    /**
     * @var FactoryService
     */
    private FactoryService $factoryService;

    public function __construct(FactoryService $factoryService)
    {

        $this->factoryService = $factoryService;
    }

    /**
     * @param  ListeRepository     $listeRepository
     * @param  NormalizerInterface $normalizer
     * @param  int                 $id
     * @param  UserInterface       $user
     * @return Response
     * @throws ExceptionInterface|Exception
     */
    public function getList(ListeRepository $listeRepository,
        NormalizerInterface $normalizer,
        int $id, UserInterface $user
    ):Response {
        try {
            $list = self::listToConsult($listeRepository, $user, $id);
            if (!empty($list) && !is_null($list)) {
                $response = $this->factoryService->responseFactory($this->factoryService->listFactory($list, $normalizer), 200);
            }else{
                $message = "Cette liste n'existe pas, ou vous n'y avez pas accès, bref vous êtes une merde.";
                $response = $this->factoryService->responseFactory($message, 403);
            }
        }catch (\Exception $exception){
            $message = "Il y a eu un problème";
            $response = $this->factoryService->responseFactory($message, 410);
        } finally {
            return $response;
        }
    }

    /**
     * @param  ListeRepository     $listeRepository
     * @param  NormalizerInterface $normalizer
     * @return Response
     * @throws ExceptionInterface
     */
    public function getAllLists(ListeRepository $listeRepository,
        NormalizerInterface $normalizer
    ):Response {
        try {
            $lists = $listeRepository->findAll();
            if (!empty($lists) && !is_null($lists)) {
                $response = $this->factoryService->responseFactory($this->factoryService->listsFactory($lists, $normalizer), 200);
            }else{
                $message = "Il n'existe aucune listes";
                $response = $this->factoryService->responseFactory($message, 403);
            }
        }catch (\Exception $exception){
            $message = "Il y a eu un problème";
            $response = $this->factoryService->responseFactory($message, 410);
        } finally {
            return $response;
        }
    }

    /**
     * @param  Request             $request
     * @param  NormalizerInterface $normalizer
     * @param  $entityManager
     * @param  UserInterface       $createdBy
     * @param  ListTypeRepository  $listTypeRepository
     * @return Response
     * @throws ExceptionInterface
     */
    public function newList(Request $request,
        NormalizerInterface $normalizer,
        $entityManager,
        UserInterface $createdBy,
        ListTypeRepository $listTypeRepository
    ) {
        $data = json_decode($request->getContent(), true);
        try {
            $listType = $listTypeRepository->findOneBy(['id'=>intval($data['idListType'])]);
            $list = new liste($createdBy, new DateTime(), $listType, $data['content']);
            $entityManager->persist($list);
            $entityManager->flush();
            $response = $this->factoryService->responseFactory($this->factoryService->listFactory($list, $normalizer), 200);
        }catch (\Exception $exception){
            $message = "Il y a eu un problème";
            $response = $this->factoryService->responseFactory($message, 410);
        } finally {
            return $response;
        }
    }

    /**
     * @param Request $request
     * @param ListeRepository $listeRepository
     * @param NormalizerInterface $normalizer
     * @param  $entityManager
     * @param ListTypeRepository $listTypeRepository
     * @param int $id
     * @param User $user
     * @return Response
     * @throws ExceptionInterface
     */
    public function editList(Request $request,
        ListeRepository $listeRepository,
        NormalizerInterface $normalizer,
        $entityManager,
        ListTypeRepository $listTypeRepository,
        int $id,
                             User $user
    ):Response {
        try {
            $data = json_decode($request->getContent(), true);
            $list = $listeRepository->findOneBy(['id'=>$id]);
            if (!empty($list) && !is_null($list)) {
                if ($list->getCreatedBy()->getEmail() === $user->getEmail()){
                    $list->setContent($data['content']);
                    $list->setListType($listTypeRepository->findOneBy(['id'=>intval($data['idListType'])]));

                    $entityManager->flush();
                    $response = $this->factoryService->responseFactory($this->factoryService->listFactory($list, $normalizer), 200);
                }else{
                    $message = "Vous ne pouvez pas modifier cette liste car vous n'en êtes pas l'auteur/autrice";
                    $response = $this->factoryService->responseFactory($message, 403);
                }
            }else{
                $message = "Il n'existe pas de liste sour cet Identifiant";
                $response = $this->factoryService->responseFactory($message, 404);
            }
        }catch (\Exception $exception){
            $message = "Il y a eu un problème";
            $response = $this->factoryService->responseFactory($message, 410);
        } finally {
            return $response;
        }
    }

    /**
     * @param ListeRepository $listeRepository
     * @param  $entityManger
     * @param int $id
     * @param User $user
     * @return Response
     */
    public function deleteList(ListeRepository $listeRepository, $entityManger, int $id, User $user):Response
    {
        try {
            $list = $listeRepository->findOneBy(['id'=>$id]);
            if (!empty($list)&&!is_null($list)) {
                if ($list->getCreatedBy()->getEmail() === $user->getEmail()){
                    $entityManger->remove($list);
                    $entityManger->flush();
                    $message = "La liste a été supprimée";
                    $response = $this->factoryService->responseFactory($message, 200);
                }else{
                    $message = "Vous ne pouvez pas supprimer cette liste car vous n'en êtes pas l'auteur/autrice";
                    $response = $this->factoryService->responseFactory($message, 403);
                }
            }else{
                $message = "Il n'existe pas de liste sous cet identifiant";
                $response = $this->factoryService->responseFactory($message, 200);
            }
        }catch (\Exception $exception){
            $message = "Il y a eu un problème";
            $response = $this->factoryService->responseFactory($message, 410);
        } finally {
            return $response;
        }
    }

    /**
     * @param  ListeRepository     $listeRepository
     * @param  User                $user
     * @param  NormalizerInterface $normalizer
     * @return Response
     * @throws Exception
     * @throws ExceptionInterface
     */
    public function getListsByUser(ListeRepository $listeRepository,
        User $user,
        NormalizerInterface $normalizer
    ):Response {
        try {
            $arrayLists = $listeRepository->findListsByUser($user);
            $lists = self::arrayToObjectsLists($arrayLists, $listeRepository);
            if (!empty($lists) && !is_null($lists)) {
                $response = $this->factoryService->responseFactory($this->factoryService->listsFactory($lists, $normalizer), 200);
            }else{
                $message = "Vous n'avez pas de listes enregistrées.";
                $response = $this->factoryService->responseFactory($message, 200);
            }
        }catch (\Exception $exception){
            $message = "Il y a eu un problème.";
            $response = $this->factoryService->responseFactory($message, 400);
        } finally {
            return $response;
        }
    }

    /**
     * @param  array           $lists
     * @param  ListeRepository $listeRepository
     * @return array
     */
    private static function arrayToObjectsLists(array $lists, ListeRepository $listeRepository):array
    {
        $listsFiled = [];
        foreach ($lists as $list){
            $listsFiled[] = $listeRepository->findOneBy(['id'=>$list['id']]);
        }
        return $listsFiled;
    }

    /**
     * @param  ListeRepository $listeRepository
     * @param  UserInterface   $user
     * @param  int             $id
     * @return Liste|null
     * @throws Exception
     * @throws \Doctrine\DBAL\Exception
     */
    private static function listToConsult(ListeRepository $listeRepository, UserInterface $user, int $id): ?Liste
    {
        $result = null;
        $arrayLists = $listeRepository->findListsByUser($user);
        $lists = self::arrayToObjectsLists($arrayLists, $listeRepository);
        $list = $listeRepository->findOneBy(["id"=>$id]);
        if (in_array($list, $lists)) {
            $result = $list;
        }
        return $result;
    }
}