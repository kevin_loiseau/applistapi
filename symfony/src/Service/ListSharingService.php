<?php


namespace App\Service;


use App\Entity\Liste;
use App\Entity\User;
use App\Mailing\Mailer;
use App\Repository\FriendShipRepository;
use App\Repository\ListeRepository;
use App\Repository\ListSharedWithRepository;
use App\Repository\ShareWayRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use App\Entity\ListSharedWith;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;

class ListSharingService
{
    /**
     * @var FactoryService
     */
    private FactoryService $factoryService;

    /**
     * @var FriendShipService
     */
    private FriendShipService $friendShipService;

    /**
     * ListSharingService constructor.
     *
     * @param FactoryService    $factoryService
     * @param FriendShipService $friendShipService
     */
    public function __construct(FactoryService $factoryService, FriendShipService $friendShipService)
    {
        $this->factoryService = $factoryService;
        $this->friendShipService = $friendShipService;
    }

    /**
     * @param  Request                  $request
     * @param  NormalizerInterface      $normalizer
     * @param  UserRepository           $userRepository
     * @param  ListSharedWithRepository $listSharedWithRepository
     * @param  ShareWayRepository       $shareWayRepository
     * @param  ListeRepository          $listeRepository
     * @param  FriendShipRepository     $friendShipRepository
     * @param  $entityManager
     * @param  UserInterface            $sharedFrom
     * @param  Mailer                   $mailer
     * @return Response
     */
    public function newListSharing(Request $request,
        NormalizerInterface $normalizer,
        UserRepository $userRepository,
        ListSharedWithRepository $listSharedWithRepository,
        ShareWayRepository $shareWayRepository,
        ListeRepository $listeRepository,
        FriendShipRepository $friendShipRepository,
        $entityManager,
        UserInterface $sharedFrom,
        Mailer $mailer
    ) {
        try {
            $data = json_decode($request->getContent(), true);
            $sharedUsers = $data['sharedTo'];
            $message = null;
            $code = null;
            foreach ($sharedUsers as $sharedToEmail){
                $allowDetails = self::isAllowToShareList(
                    $userRepository,
                    $shareWayRepository,
                    $friendShipRepository,
                    $listeRepository,
                    $listSharedWithRepository,
                    $sharedFrom, $data,
                    $sharedToEmail
                );

                if ($allowDetails[0]) {
                    $message .= $allowDetails[1] .". ";
                    $code = $allowDetails[2];
                    $listSharing = $allowDetails[3];
                    $entityManager->persist($listSharing);
                    $entityManager->flush();
                    if ($listSharing->getListSharedVia()->getWay() === "email") {
                        $mailer->notify($listSharing);
                    }
                }else {
                    $message .= $allowDetails[1] . ". ";
                    $code = $allowDetails[2];
                }
            }
            $response = $this->factoryService->responseFactory($message, $code);
        }catch (\Exception $exception){
            $message = "Il y a eu un problème";
            $response = $this->factoryService->responseFactory($message, 410);
        } finally {
            return $response;
        }
    }

    /**
     * Obtention du droit au partage d'une liste si :
     *  - L'uilisateur à qui on partage la liste existe
     *  - Le moyen de partage existe
     *  - La liste en elle même existe
     *  - L'utilisateur n'envoie pas une liste à lui-même
     *  - L'utilisateur est ami et a une relation active avec le l'utilisateur à qui il partage la liste
     *  - L'utilisateur est le créateur de la liste
     *  - L'utilisateur partage une liste qui n'a pas déjà été partagé ou une liste qui a été partagé mais par email seulement.
     *
     * @param  UserRepository           $userRepository
     * @param  ShareWayRepository       $shareWayRepository
     * @param  FriendShipRepository     $friendShipRepository
     * @param  ListeRepository          $listeRepository
     * @param  ListSharedWithRepository $listSharedWithRepository
     * @param  UserInterface            $sharedFrom
     * @param  array                    $data
     * @param  string                   $sharedToEmail
     * @return array
     * @throws NonUniqueResultException
     */
    private function isAllowToShareList(
        UserRepository $userRepository,
        ShareWayRepository $shareWayRepository,
        FriendShipRepository $friendShipRepository,
        ListeRepository $listeRepository,
        ListSharedWithRepository $listSharedWithRepository,
        UserInterface $sharedFrom,
        array $data,
        string $sharedToEmail
    ):array {
        $sharedTo = $userRepository->findOneBy(['email'=>$sharedToEmail]);
        if (!is_null($sharedTo)) {
            $sharedVia = $shareWayRepository->findOneBy(['way'=>$data['sharedWay']]);
            if (!is_null($sharedVia)) {
                $list = $listeRepository->findOneBy(['id'=>intval($data['list'])]);
                if (!is_null($list)) {
                    $listSharing = new ListSharedWith($sharedFrom, $sharedTo, $sharedVia, $list);
                    if ($listSharing->getSharedFrom() !== $listSharing->getSharedTo()) {
                        if ($this->friendShipService->verifyFriendShip($listSharing->getSharedFrom(), $listSharing->getSharedTo(), $friendShipRepository)) {
                            if ($listSharing->getListShared()->getCreatedBy() === $listSharing->getSharedFrom()) {
                                if (!self::existSharingList($listSharing, $listSharedWithRepository)
                                    || (self::existSharingList($listSharing, $listSharedWithRepository) && $listSharing->getListSharedVia()->getWay() === "email")
                                ) {
                                    $isAllowed = [true, "La liste a été partgée avec succès à ".$sharedTo.". ", 200, $listSharing];
                                }else{
                                    $isAllowed = [
                                        false,
                                        "Vous déjà partagé cette liste à ".$listSharing->getSharedTo()." par ".$listSharing->getListSharedVia()->getWay().". ",
                                        403
                                    ];
                                }
                            }else{
                                $isAllowed = [false, "Vous n'êtes pas le/la créateur/créatrice de la liste, vous ne pouvez pas partager cette liste. ", 403];
                            }
                        }else{
                            $isAllowed = [false, "Vous n'êtes pas amis, faite votre demande à ".$listSharing->getSharedTo().". ", 403];
                        }
                    }else{
                        $isAllowed = [false, "Attention vous ne pouvez pas partager cette liste avec vous-même. ", 403];
                    }
                }else{
                    $isAllowed = [false, "Le partage a échoué car il n'existe pas de liste sous cet identifiant. ", 403];
                }
            }else{
                $isAllowed = [false, "Le partage a échoué car le moyen de partage par \"".$sharedVia."\" n'est pas encore disponible. ", 403];
            }
        }else{
            $isAllowed = [false, "Le partage a échoué car il n'existe pas d'utilisateur avec l'email ".$sharedTo.". ", 403];
        }
        return $isAllowed;
    }

    /**
     * Vérification de l'existance d'un partage
     *
     * @param  ListSharedWith           $listSharedWith
     * @param  ListSharedWithRepository $listSharedWithRepository
     * @return bool
     */
    public function existSharingList(ListSharedWith $listSharedWith, ListSharedWithRepository $listSharedWithRepository)
    {
        $alreadySharedList = $listSharedWithRepository->findBy(
            ["sharedTo"=>$listSharedWith->getSharedTo(),
                "sharedFrom"=>$listSharedWith->getSharedFrom(),
                "listSharedVia"=>$listSharedWith->getListSharedVia(),
                "listShared"=>$listSharedWith->getListShared()
            ]
        );
        if (is_null($alreadySharedList) || empty($alreadySharedList)) {
            $exist = false;
        }else{
            $exist = true;
        }
        return $exist;
    }
}