<?php
namespace App\Service;

use App\Entity\User;
use App\Mailing\Mailer;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\HttpFoundation\Response;

class RegistrationService
{

    private FactoryService $factoryService;

    /**
     * RegistrationService constructor.
     *
     * @param FactoryService $factoryService
     */
    public function __construct(FactoryService $factoryService)
    {

        $this->factoryService = $factoryService;
    }

    /**
     * @param  Request                      $request
     * @param  UserRepository               $userRepository
     * @param  $entityManager
     * @param  UserPasswordEncoderInterface $passwordEncoder
     * @param  NormalizerInterface          $normalizer
     * @param  Mailer                       $mailer
     * @throws ExceptionInterface
     * @return Response
     */
    public function newRegistration(Request $request,
        UserRepository $userRepository,
        $entityManager,
        UserPasswordEncoderInterface $passwordEncoder,
        NormalizerInterface $normalizer,
        Mailer $mailer
    ): Response {
        $data = $request->getContent();

        try {
            $newUser = json_decode($data);
            $firstName = $newUser->firstname;
            $lastName = $newUser->lastname;
            $email = $newUser->email;
            $password = $newUser->password;

            $user = $userRepository->findOneBy(
                [
                'email' => $email
                ]
            );

            if (!is_null($user)) {
                $message = "Il existe déjà un utilisateur pour cette adresse mail";
                return $this->factoryService->responseFactory($message, 401);
            }

            $user = new User();
            $user->setFirstName($firstName);
            $user->setLastName($lastName);
            $user->setEmail($email);
            $user->setPassword(
                $passwordEncoder->encodePassword($user, $password)
            );
            $user->setRoles(["ROLE_USER"]);
            $entityManager->persist($user);
            $entityManager->flush();

            $mailer->registrationConfirmation($user);
            $response = $this->factoryService->responseFactory($this->factoryService->userFactory($user, $normalizer), 200);
        } catch (\Exception $exception){
            $message = "Un problème a été rencontré, réessayez ultérieurement.";
            $response = $this->factoryService->responseFactory($message, 410);
        }
        return $response;
    }
}