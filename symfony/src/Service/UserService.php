<?php

namespace App\Service;
use App\Repository\UserRepository;

class UserService
{
    /**
     * @param array $ids
     * @param UserRepository $userRepository
     * @return array
     */
    public static function arrayIdToArrayUsers(array $ids, UserRepository $userRepository):array {
        $users = [];
        foreach ($ids as $id){
            $users[] = $userRepository->findOneBy(['id'=>$id]);
        }
        return $users;
}

}